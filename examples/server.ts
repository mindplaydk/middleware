import { host } from "../src/host";
import { Middleware, Request, Response } from "../src/http";
import { logger } from "../src/logger-middleware";
import http from "http";
import { compose } from "../src/compose";
import { html, text } from "../src/response";
import { controller } from "../src/controller";
import Schema, { string, number, array } from 'computed-types';
import { parseJSON } from "../src/body-parser";

async function helloWorld(request: Request): Promise<Response> {
  return html(`<!DOCTYPE html>
  <html>
  <head>
    <title>Test</title>
    <script>
      async function login() {
        const post = {
          name: "Bob",
          username: "bob123",
          status: "active",
          items: [{ id: "ABC123", amount: 2 }]
        };

        const body = JSON.stringify(post);

        const result = await fetch("/login", { method: "POST", body });

        console.log(await result.text());
      }

      async function badness() {
        const post = {
          name: 123,
          oh_no: ["bad"],
        };

        const body = JSON.stringify(post);

        const result = await fetch("/login", { method: "POST", body });

        console.log(await result.text());
      }
      </script>
  </head>
  <body>
    <button onclick="login()">Post some good stuff</button>
    <button onclick="badness()">Post some bad stuff</button>
    <p>(results in console)</p>
  </body>
  </html>
  `);
}

const login = controller(
  parseJSON,
  {
    name: string.trim().normalize().between(3, 40).optional(),
    username: /^[a-z0-9]{3,10}$/,
    status: Schema.either('active' as const, 'suspended' as const),
    items: array
      .of({
        id: string,
        amount: number.gte(1).integer(),
      })
      .min(1),
  },
  async input => {
    return text(`OK! ${JSON.stringify(input)}`);
  },
  async error => {
    return text(`ERROR! ${JSON.stringify(error)}`);
  }
);

const helloMiddleware: Middleware = handle => request => {
  console.log(`hello! ${request.url.pathname}`)

  if (request.url.pathname === "/login") {
    return login(request);
  }

  return handle(request);
}

const stack = [
  logger(),
  helloMiddleware,
];

const handler = compose(stack, helloWorld);

const app = host(handler);

const server = http.createServer(app).listen(
  { port: 8080 },
  () => { console.log(`server running`) }
);
