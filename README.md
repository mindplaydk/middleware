## Middleware Sandbox

This is a toy project to experiment with purely functional `request => response` style middleware
in Node.JS, adopting some of PHP's PSR standards, specifically:

  * PSR-3, Logging: `logging.ts`
  * PSR-7/15, HTTP models, handlers and middleware: `http.ts`
  * PSR-17, Factories: TODO
  * PSR-11, Container: TODO
  * PSR-18, Client: TODO

To run the test:

    npm run test

To run the example server:

    npm run server
