import { Handler, Method } from "./http";
import { RequestListener } from "http";

export function host(handler: Handler): RequestListener {
  return async ($request, $response) => {
    const buffers = [];

    {
      let chunk: Buffer;

      for await (chunk of $request) {
        buffers.push(chunk);
      }
    }
  
    const body = Buffer.concat(buffers);

    const target = $request.url!;

    const url = new URL(target, `http://${$request.headers.host}`);

    const method = $request.method! as Method;

    const headers = Object.fromEntries(
      Object.entries($request.headers).map(
        ([name, value]) => [name, Array.isArray(value) ? value : [value!]]
      )
    );
  
    const response = await handler({
      url,
      target,
      method,
      headers,
      body
    });

    $response.writeHead(
      response.status,
      response.reason,
      Object.fromEntries(
        Object.entries(response.headers).map(
          ([name, value]) => [name, [...value]]
        )
      )
    );

    function write(data: Buffer | string, encoding?: BufferEncoding): void {
      $response.write(data, encoding || "utf-8")
    }

    await response.writer(write);

    return new Promise(resolve => { $response.end(resolve); });
  }
}
