export type Method = "GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "CONNECT" | "OPTIONS" | "TRACE" | "PATCH";

/** async function that writes to the body response stream */
export type Writer = (write: Write) => Promise<void>;

/** function exposed to a Writer, enabling it to write to the body response stream */
export type Write = {
  /**
   * Appends a string of `data` using the specified `encoding` (or UTF-8 by default)
   */
  (data: string, encoding?: BufferEncoding): void;
  /**
   * Appends a `Buffer` of `data` to the response
   */
  (data: Buffer): void;
};

// TODO gotta do something about this header type - it's extremely error prone to
//      work with... because a string literal "xyz" is actually assignable to
//      Iterable<string> (because you can iterate over characters) an incorrect
//      assignment such as { "set-cookie": "fudge" } is currently valid! 😐

/** collection of headers */
export type Headers = { [name: string]: Iterable<string> };

// TODO For now, no common Message model, as Request and Response currently don't have
//      much in common - to avoid the mutability problem with Response body, I went
//      with an immutable Writer for the Response body instead of a Buffer.
//
//      For the Request, I did go with a body Buffer for now, but this is just a
//      temporary solution to get something going - in practice, this wouldn't hold
//      up for things like large file uploads. The body needs to be a stream in both
//      cases, but since you won't be writing to the Request body, we don't have the
//      same mutability issue we have with the Response body, which you will of course
//      be writing to - so they really may end up having two different types...

// TODO I've omitted the HTTP protocol version field from the Request model for now -
//      I'm not convinced it's meaningful? We ultimately would want to be able to
//      host the same application under an HTTP/1 or HTTP/2 server without, and it's
//      not really clear how or when this information is useful in the model...

export interface Request {
  readonly url: URL;
  readonly target: string;
  readonly method: Method;
  readonly headers: Headers;
  readonly body: Buffer;
}

export interface Response {
  readonly status: number;
  readonly reason: string;
  readonly headers: Headers;
  readonly writer: Writer;
}

/** async function that accepts a Request and produces a Response */
export type Handler = (request: Request) => Promise<Response>;

/** function that accepts a Handler and produces a decorated Handler */
export type Middleware = (handler: Handler) => Handler;

/** read-only list of Middleware */
export type MiddlewareStack = Iterable<Middleware>;
