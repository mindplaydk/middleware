/*

TODO

content negotiation is important for lots of reasons - for example, we need it
in order to correctly interpret `Accept-Encoding` request-headers, so we can
select an appropriate type of response compression.

`negotiator` is the most popular package:

https://www.npmjs.com/package/negotiator

judging by the numbers, practically every library uses this convenience layer:

https://www.npmjs.com/package/accepts

this might also be worth a look - it looks simpler:

https://www.npmjs.com/package/negotiated

*/
