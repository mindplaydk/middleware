/*

TODO

`co-body` is a good source of reference:

https://www.npmjs.com/package/co-body

it directly integrates decompression, which may not be the way to go - we probably
want this at a lower level of abstraction, e.g. built into the request body model
somehow. (likely differentiating `body` from `rawBody`, so that the raw body can
still be accessed directly - e.g. for reverse/proxy or CDN/caching scenarios, where
you just want to forward the compressed body for some reason.)

*/

import { Request } from "./http";

export function parseJSON(request: Request) {
  return JSON.parse(request.body.toString("utf-8"));
}
