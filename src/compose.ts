import { Handler, MiddlewareStack } from "./http";

/**
 * Composes middleware into a handler
 */
export function compose(stack: MiddlewareStack, finalHandler: Handler): Handler {
  let handler = finalHandler;

  const middlewares = [...stack];

  for (let i = middlewares.length; i--;) {
    handler = middlewares[i](handler);
  }

  return handler;
}
