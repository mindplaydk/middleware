import { Middleware } from "./http";
import { Logger } from "./logging";

export function logger(log = toConsole): Middleware {
  return (innerHandler) => {
    return async (request) => {
      try {
        const response = await innerHandler(request);
        
        log(`[${request.method}] ${response.status} ${request.url}`, "info");

        return response;
      } catch (error: any) {
        log(`[${request.method}] ${error}`, "error");

        throw error;
      }
    }
  };
}

export const toConsole: Logger = (message, severity) => {
  console.log(`[${severity}] ${message}`);
}
