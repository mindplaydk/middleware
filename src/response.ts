import { Response, Headers, Write, Writer } from "./http";
import http from "http";

export function content(body: string, mimeType: string): Response {
  return {
    status: 200,
    reason: "OK",
    headers: {
      "content-type": [mimeType]
    },
    writer: async write => write(body, "utf-8")
  }
}

export const text = (body: string) => content(body, "text/plain");

export const html = (html: string) => content(html, "text/html; charset=utf-8");

export function applyStatus(response: Omit<Response, "status" | "reason">, status: number, reason?: string): Response {
  return {
    ...response,
    status,
    reason: reason || http.STATUS_CODES[status] || "unknown",
  }
}

export function applyHeaders(response: Omit<Response, "headers">, headers: Headers): Response {
  return {
    ...response,
    headers
  }
}

export function addHeader(response: Response, name: string, value: string): Response {
  return {
    ...response,
    headers: {
      ...response.headers,
      [name]: [...response.headers[name] || []].concat(value)
    }
  }
}

export function replaceHeader(response: Response, name: string, value: string): Response {
  return {
    ...response,
    headers: {
      ...response.headers,
      [name]: [value]
    }
  }
}

export async function toBuffer(writer: Writer): Promise<Buffer> {
  let buffer = Buffer.from("");

  function write(data: Buffer | string, encoding: BufferEncoding = "utf-8"): void {
    buffer = Buffer.concat([
      buffer,
      Buffer.isBuffer(data)
        ? data
        : Buffer.from(data, encoding)
    ]);
  }

  await writer(write);

  return buffer;
}
