import Schema, { Type, ValidationError } from 'computed-types';
import { Handler, Request, Response } from './http';

export function controller<T>(
  parse: (request: Request) => object,
  schema: T,
  accept: (input: Type<T>) => Promise<Response>,
  reject: (error: ValidationError) => Promise<Response>
): Handler {
  const validate = Schema(schema).destruct() as any; // TODO proper types
  
  return async request => {
    const [error, input] = await validate(parse(request));

    if (error) {
      return reject(error); // TODO proper error handling
    } else {
      return accept(input);
    }
  }
}
