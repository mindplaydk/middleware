export type Severity = "debug" | "info" | "notice" | "warning" | "error" | "critical" | "alert" | "emergency";

export type Logger = (message: string, severity: Severity, context?: unknown) => void;
