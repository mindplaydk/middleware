/*

TODO

decompression is required for requests - and compression is required for
responses, but must be kept optional. (response compression must be cached.)

`zlib` is built into Node and supports all common forms of compression - there
are examples in the documentation here:

https://nodejs.org/api/zlib.html#compressing-http-requests-and-responses

as noted in the server example, "not a conformant accept-encoding parser"
without some form of content negotation - see `negotiation.ts`.

*/
