import { hold, report, test } from "zora";
import { createDiffReporter } from "zora-reporters";
import { compose } from "../src/compose";
import { Middleware } from "../src/http";
import { content, toBuffer, text } from "../src/response";

hold();

test(`can create Buffer from Writer`, async is => {
  const buffer = await toBuffer(async write => {
    write("A");
    write(Buffer.from("B"));
  });

  is.equal(
    buffer.toString("utf-8"),
    "AB",
    "can write strings or Buffers"
  );
});

test(`can create content response`, async is => {
  const body = "{}";
  const mimeType = "application/json";

  const response = content(body, mimeType);

  is.equal(response.status, 200);
  is.equal(response.reason, "OK");

  is.equal(
    (await toBuffer(response.writer)).toString("utf-8"),
    body
  )
});

// TODO tests for response builders and modifiers in response.ts

test(`can compose middleware`, async is => {
  const messages: string[] = [];

  function makeMiddleware(name: string): Middleware {
    return handler => request => {
      messages.push(`before ${name}`);

      const response = handler(request);
      
      messages.push(`after ${name}`);

      return response;
    }
  }

  const A = makeMiddleware("A");
  const B = makeMiddleware("B");

  const expectedResponse = text("Hello World");

  const composed = compose([A, B], async request => {
    messages.push("FINAL");

    return expectedResponse;
  });

  const response = await composed({
    body: Buffer.from(""),
    url: new URL("http://localhost/"),
    target: "/",
    method: "GET",
    headers: {}
  });

  is.equal(response, expectedResponse, "it runs the final handler");

  is.equal(
    messages,
    [
      "before A",
      "before B",
      "FINAL",
      "after B",
      "after A",
    ],
    "it should run composed middleware in the expected order"
  )
});

report({ reporter: createDiffReporter() })
